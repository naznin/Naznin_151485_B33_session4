<?php
//addslashes
$str = "is your  name j'ohn?";
$str1='i will not go "there"';
$str2="thank \you";
echo addslashes($str);//outputs is your name j\'ohn?
echo "</br>";
echo addslashes($str1);//outputs i will not go \"there\"
echo "</br>";
echo addslashes($str2);//outputs thank \\you
echo "</br>";
?>

<?php
// explode and join
$class_list=" V,VI,VII,VIII,IX,X";
$class_array = explode (",",$class_list);//spilts the input string and return as array
print_r($class_array);
echo "</br>";
$class_string=join(",",$class_array);//join the input array and return as sring
echo $class_string;
echo "</br>";

?>

<?php
//htmlentities
$convert = htmlentities ("<li><ahref='index.php'>we are learning PHP</a></li>",ENT_COMPAT);
echo $convert;
?>
<?php
//join_string
echo "</br>";
$array_name= array('First name', 'Middle name', 'Last name');
$join_string = implode("-",$array_name);
echo $join_string ;
?>
<?php
//ltrim()
echo "</br>";
$string="\t\tpondit.com\t\t";
$trim_string=ltrim($string);//removes \t\t or spaces
var_dump($trim_string);//shows number of character in trimmed string and the trimmed string
echo "</br>";
$string1="good morning";
$trim_string1=ltrim($string1,'g');//removes g from left side
var_dump($trim_string1);//shows number of character in trimmed string and the trimmed string
//rtrim
echo "</br>";
$trim_string=rtrim($string);//removes \t\t
var_dump($trim_string);//shows number of character in trimmed string and the trimmed string
echo "</br>";
$trim_string1=rtrim($string1,'g');//removes gfrom right side
var_dump($trim_string1);//shows number of character in trimmed string and the trimmed string
echo "</br>";
//trim
echo "</br>";
$trim_string=trim($string);//removes \t\t
var_dump($trim_string);//shows number of character in trimmed string and the trimmed string
echo "</br>";
$trim_string1=trim($string1,'g');//removes g from both side
var_dump($trim_string1);//shows number of character in trimmed string and the trimmed string
echo "</br>";
?>


<?php
//nl2br
echo nl2br("good \n morning");
?>


<?php
echo "</br>";
echo str_repeat("PHP programming </br>",5);
?>

<?php
$string_name="what's your name ?";
echo "</br>";
print_r(str_split($string_name));
echo "</br>";
print_r(str_split($string_name,4));
?>


<?php
//strip_tags
echo "</br>";
$input_string="<b>welcome</b>";
echo 'before strip:'.$input_string.'</br>';
echo 'after strip:'.strip_tags($input_string);
?>

<?php
//strlen
echo "</br>";
$string_name='welcome to pondit.com';
echo strlen($string_name);
?>
<?php
//strtolower
$string1='GOOD MORNING';
echo '</br>';
echo strtolower($string1);
?>
<?php
//strtoupper
$string1='good morning';
echo '</br>';
echo strtoupper($string1);
?>
<?php
//substr_compare
echo "</br>";
echo substr_compare("abcde","bc",1,2);
?>

<?php
//substr_count
$text='this is a text';
echo strlen($text);
echo substr_count ($text,'is');
echo substr_count ($text,'is',3);
?>


<?php
//substr_replace
$string='abcdefgh';
echo "</br>";
echo substr_replace($string,'bob',2,4);
?>
<?php
str_pad
echo "</br>";
$str="Hello World";
echo str_pad($str,20,".",STR_PAD_BOTH);//pads hello world with dots in both sides
?>


<?php
//ucfirst
echo "</br>";
$str="welcome";
echo ucfirst($str);//converts w to W
?>

<?php
//str_replace
echo str_replace("world","peter","Hello world !");//replaces world with peter
?>

